//
//  QuestionViewController.swift
//  QRCodeReader
//
//  Created by Sai Sri Lakshmi on 17/04/19.
//  Copyright © 2019 AppCoda. All rights reserved.
//

import UIKit

class QuestionViewController: UIViewController {
    @IBOutlet weak var questionLBL: UILabel!
    
    @IBOutlet weak var submitBTN: UIButton!
    
    @IBOutlet weak var cancelBtn: UIButton!
    
    @IBOutlet weak var answerTF: UITextField!
    override func viewWillAppear(_ animated: Bool) {
//    questionLBL.text! = Problems.shared.text
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        questionLBL.text = QRScannerController.decodedURLString
        // Do any additional setup after loading the view.
    }
    
    @IBAction func submitAnswer(_ sender: Any) {
           
            if Problems.shared.curretProblem?.answer == answerTF.text!{
                Problems.shared.points = Problems.shared.points + 10
                Problems.shared.text = "You are right"
            }
            //self.dismiss(animated: false, completion: {})
    }
        /*
        // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
