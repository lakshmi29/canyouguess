//
//  ClueViewController.swift
//  CanYou
//
//  Created by Student on 4/19/19.
//  Copyright © 2019 Student. All rights reserved.
//

import UIKit

class ClueViewController: UIViewController {

    var selectedClueIdx:Int!
    @IBOutlet weak var clueLBL: UITextView!
    @IBOutlet weak var answerTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        clueLBL.text = ClueManager.shared.scanedClue
    }
    var qanswerdict: [String:String]  = ["This element has the lowest melting point of all the elements!!":"helium", "I am the most simple element. I usually have no neutrons, one proton, and a single electron. I am the most abundant element, and I am responsible for the good and the bad-I am the primary fuel source of the sun, but it was me that caused the Hindenburg zeppelin to burn so rapidly. What element am I?":"hydrogen" ]
    
    
    @IBAction func verifyAction(_ sender: Any) {
        //If answer matches the element given, then give the alert
        if qanswerdict[clueLBL.text!] == answerTF.text! {
            alert(title: "Success", alertMessage: "Correct Answer!", style: .default, handler: {(act) in
                self.dismiss(animated: true, completion: nil)
                return})
            ScoreManager.shared.addScore(+1)
        }

         else {
            //If the answer doesn't match the element given, then give the "failed" alert
            alert(title: "Failed", alertMessage: "Sorry, incorrect Answer.", style: .destructive, handler: {(act) in
                self.dismiss(animated: true, completion: nil)
                return})
            ScoreManager.shared.addScore(-1)
        }
    }
    
    func alert(title:String, alertMessage:String, style:UIAlertAction.Style, handler:((UIAlertAction)->Void)?){
        let alertController = UIAlertController(title: title, message: alertMessage,
                                                preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: style, handler: handler)
        alertController.addAction(action)
        self.show(alertController, sender: nil)
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        ClueManager.shared.scanedClue = nil
        self.dismiss(animated: false, completion: {})
    }

}
