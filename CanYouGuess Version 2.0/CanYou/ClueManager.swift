//
//  ClueManager.swift
//  CanYou
//
//  Created by Student on 4/19/19.
//  Copyright © 2019 Student. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class ClueManager {
    var context:NSManagedObjectContext!
    var appDelegate:AppDelegate!
    private init(){
        context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        appDelegate = UIApplication.shared.delegate as? AppDelegate
        fetchData()
    }
    
    static var shared = ClueManager()
    
    var scanedClue:String!
    var unlockedClues:[UnlockedClues] = []
    var unlockedElements:[UnlockedElements] = []
    var score:Score!
    
    func fetchData() {
        let r1:NSFetchRequest<UnlockedClues> = NSFetchRequest(entityName: "UnlockedClues")
        
        let r2:NSFetchRequest<UnlockedElements> = NSFetchRequest(entityName: "UnlockedElements")
        r2.sortDescriptors = [NSSortDescriptor(key: "elementNum", ascending: true)]
        
        let r3:NSFetchRequest<Score> = NSFetchRequest(entityName: "Score")
        
        if let data = try? context.fetch(r1) { unlockedClues = data }
        if let data = try? context.fetch(r2) { unlockedElements = data }
        if let data = try? context.fetch(r3) {
            if data.count > 0 {
                score = data[0]
            } else {
                let s = NSEntityDescription.insertNewObject(forEntityName: "Score", into: context) as! Score // create a River object in the context
                s.currentScore = 0
                appDelegate.saveContext()
            }
        }
    }
    
    func verify(answer:String, forClueAt clueIdx:Int) -> Bool{
        return false
    }
    
    
    func archiveClue(clue:String){
        let r:NSFetchRequest<UnlockedClues> = NSFetchRequest(entityName: "UnlockedClues")
        let predicate = NSPredicate(format: "clue = %@", clue)
        
        r.predicate = predicate
        if let data = try? context.fetch(r) {
            if data.count == 0 {
                let c = NSEntityDescription.insertNewObject(forEntityName: "UnlockedClues", into: context) as! UnlockedClues // create a River object in the context
                c.clue = clue
                unlockedClues.append(c)
                appDelegate.saveContext()
            }
        }
    }
}

