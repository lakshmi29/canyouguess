

import UIKit

class ElementCell: UICollectionViewCell {
    
    //creating labels for the atomic number and symbol of each element
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var symbolLabel: UILabel!
    
}
