//
//  SecondViewController.swift
//  CanYou
//
//  Created by Student on 4/18/19.
//  Copyright © 2019 Student. All rights reserved.
//

import UIKit

class ScoreViewController: UIViewController {
    @IBOutlet weak var personalScoreLBL: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        personalScoreLBL.text = "\(ScoreManager.shared.retrieveScores())"
        // Do any additional setup after loading the view, typically from a nib.
    }
    override func viewWillAppear(_ animated: Bool) {
        personalScoreLBL.text = "\(ScoreManager.shared.retrieveScores())"
    }


}

