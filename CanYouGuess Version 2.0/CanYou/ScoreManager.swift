//
//  ScoreManager.swift
//  CanYou
//
//  Created by Sai Sri Lakshmi on 24/04/19.
//  Copyright © 2019 Student. All rights reserved.
//

import Foundation
class ScoreManager{
    var score:Int!
    static var shared = ScoreManager()
    
    init(){
        score = 0;
    }
    func addScore(_ scoreToBeAdded:Int){
        self.score = self.score + scoreToBeAdded
    }
    func retrieveScores() -> Int{
        return self.score
    }
}
