
import UIKit

class PeriodicTableViewController: UICollectionViewController, ElementDataSource {
    
    var elements: [Element]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //loads the elements into their respective places
        loadElements()
        collectionView?.collectionViewLayout = PeriodicTableLayout()
    }
    
    private func loadElements() {
        //This method will take the values in our .json file and place them in the periodic table view controller
        let fileURL = Bundle.main.url(forResource: "Elements", withExtension: "json")!
        let data = try! Data(contentsOf: fileURL)
        let json = try! JSONSerialization.jsonObject(with: data) as! [[String: Any]]
        elements = json.map { Element(dictionary: $0) }
    }
}

extension PeriodicTableViewController {
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return elements?.count ?? 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ElementCell", for: indexPath) as! ElementCell
        let element = elements![indexPath.row]
        cell.numberLabel.text = "\(element.atomicNumber)"
        cell.symbolLabel.text = element.symbol
        return cell
    }
}

