# Building a Barcode and QR code Reader in Swift
Aim:-
To teach about the elements based on their properties and behaviours using game.

Tab1 :- The QR code scanner and followed by the camera and then a popup question will come then you will answer the question and click submit.

Tab2 :- After completing the whole tab1 duties then in the second tab you will see the score of your game and then it will be giving a choice to play on otherwise you will be directed to tab3.

Tab3 :- At the end of the of game you will be directed to tab3 and you can see what elements we got unlocked by playing the game.


In tab2 the score will be added gradually depending on what elements we have unlocked.


This is a demo app showing you how to implement QR and barcode scanning in Swift. For the full tutorial, please refer to the link below:

http://www.appcoda.com/barcode-reader-swift/
